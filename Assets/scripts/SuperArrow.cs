﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuperArrow : MonoBehaviour {
   public Type myType;
   public enum Type
    {
        Fire,Water,Earth
    }

    List<Type> strengths = new List<Type>();
    List<Type> weakness = new List<Type>();

    void Start()
    {
        FillStrengthsAndWeakness();
    }

    public void FillStrengthsAndWeakness()
    {
        if(myType == Type.Fire)
        {
            strengths.Add(Type.Earth);
            weakness.Add(Type.Water);
        }
        if (myType == Type.Water)
        {
            strengths.Add(Type.Fire);
            weakness.Add(Type.Earth);
        }
        if (myType == Type.Earth)
        {
            strengths.Add(Type.Water);
            weakness.Add(Type.Fire);
        }

        
    }

    public void Fight(Type type)
    {
        if (strengths.Contains(type))
        {
            Debug.Log("i won");
            //give points, damage enemy
        }
        else if (weakness.Contains(type))
        {
            Debug.Log("I loose");
            //damage player health
        }
        else
        {
            Debug.Log("draw");
        }
    }

    void OnCollisionEnter(Collision col)
    {
        if(col.gameObject.tag == "arrow")
        {
            SuperArrow enemy = col.gameObject.GetComponent<SuperArrow>();
            Fight(enemy.myType);
        }
    }
}
