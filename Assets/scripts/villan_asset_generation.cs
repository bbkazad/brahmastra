﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class villan_asset_generation : MonoBehaviour
{

    public int woods;
    public int snakes;
    public int fires;
    public int waters;
    public int mountains;
    public int eagles;
    public int sum;

    public Sprite woodsprite;
    public Sprite snakessprite;
    public Sprite firessprite;
    public Sprite waterssprite;
    public Sprite mountainssprite;
    public Sprite eaglessprite;

    void Start()
    {
        woods = Random.Range(1, 5);
        snakes = Random.Range(1, 5);
        fires = Random.Range(1, 5);
        waters = Random.Range(1, 5);
        mountains = Random.Range(1, 5);
        eagles = Random.Range(1, 5);

        sum = woods + snakes + fires + waters + mountains + eagles;
        Debug.Log(sum);
        while (sum > 0)
        {
            int arrow_select = Random.Range(1, 6);
            switch (arrow_select)
            {
                case 1:
                    if (woods > 0)
                    {
                        this.gameObject.GetComponent<SpriteRenderer>().sprite = woodsprite;
                        woods -= 1;
                        sum -= 1;
                    }
                    break;
                case 2:
                    if (snakes > 0)
                    {
                        this.gameObject.GetComponent<SpriteRenderer>().sprite = snakessprite;
                        snakes -= 1;
                        sum -= 1;
                    }
                    break;
                case 3:
                    if (fires > 0)
                    {
                        this.gameObject.GetComponent<SpriteRenderer>().sprite = firessprite;
                        fires -= 1;
                        sum -= 1;
                    }
                    break;
                case 4:
                    if (waters > 0)
                    {
                        this.gameObject.GetComponent<SpriteRenderer>().sprite = waterssprite;
                        waters -= 1;
                        sum -= 1;
                    }
                    break;
                case 5:
                    if (mountains > 0)
                    {

                        this.gameObject.GetComponent<SpriteRenderer>().sprite = mountainssprite;
                        mountains -= 1;
                        sum -= 1;
                    }
                    break;
                case 6:
                    if (eagles > 0)
                    {
                        this.gameObject.GetComponent<SpriteRenderer>().sprite = eaglessprite;
                        eagles -= 1;
                        sum -= 1;
                    }
                    break;
            }

            StartCoroutine(waiter());
        }
    }

    IEnumerator waiter()
    {
        //Wait for 2 seconds
        yield return new WaitForSeconds(2);
    }


}