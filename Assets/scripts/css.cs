﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;
using UnityEngine.SceneManagement;


public class css : MonoBehaviour
{
    public Text playerbloodper, villanbloodper;
    // Use this for initialization
    public static int playerstrength, villanstrength;
    public Image villanblood,arrowLoad;
    public static Image playerblood;
    private Animator aklav, backGround;// changedBackground;
    public static bool facingRight;// singleSwipeCheck;
    public Transform target;
    public Transform throwPoint;
    public GameObject arrow,winScene,lossScene;
    //public Image test;
    public float timeTillHit = 1f;
    public Button wood, fire, eagle, water, mountain, snake,brahmastra;
    public GameObject woodarrow, firearrow, eaglearrow, waterarrow, mountainarrow, snakearrow;
    public GameObject brahmastraarrow,arrowmm;
    public Text c1, c2, c3, c4, c5, c6, wontext;
    public int arrowSelected;
    public SpriteState nullSprite;
    
    public static int sum;

    public Vector2 firstPressPos;
    public Vector2 secondPressPos;
    public Vector2 currentSwipe;

    
    //public evil evilobject;

    void Start()
    {
       
        aklav = GetComponent<Animator>();
        //changedBackground = GameObject.Find("ChangedBackground").GetComponent<Animator>();
        //test= GameObject.Find("test").GetComponent<Image>();
        backGround = GameObject.Find("backGround").GetComponent<Animator>();
        wood = wood.GetComponent<Button>();
        fire = fire.GetComponent<Button>();
        water = water.GetComponent<Button>();
        mountain = mountain.GetComponent<Button>();
        eagle = eagle.GetComponent<Button>();
        snake = snake.GetComponent<Button>();
        brahmastra = brahmastra.GetComponent<Button>();
        //utton click = clickk.GetComponent<Button>();
        //utton click = clickk.GetComponent<Button>();
        //click.onClick.AddListener(attacking);

        facingRight = true;
        //singleSwipeCheck = true;

        c1 = GameObject.Find("c1").GetComponent<Text>();
        c2 = GameObject.Find("c2").GetComponent<Text>();
        c3 = GameObject.Find("c3").GetComponent<Text>();
        c4 = GameObject.Find("c4").GetComponent<Text>();
        c5 = GameObject.Find("c5").GetComponent<Text>();
        c6 = GameObject.Find("c6").GetComponent<Text>();
        //wontext = GameObject.Find("wontext").GetComponent<Text>();
        villanblood = GameObject.Find("villanblood").GetComponent<Image>();
        playerblood = GameObject.Find("playerblood").GetComponent<Image>();
        arrowLoad = GameObject.Find("arrowLoad").GetComponent<Image>();
        int r1 = UnityEngine.Random.Range(7, 9);
        c1.text = r1.ToString();

        int r2 = UnityEngine.Random.Range(7, 9);
        c2.text = r2.ToString();

        int r3 = UnityEngine.Random.Range(7, 9);
        c3.text = r3.ToString();

        int r4 = UnityEngine.Random.Range(6, 7);
        c4.text = r4.ToString();

        int r5 = UnityEngine.Random.Range(6, 7);
        c5.text = r5.ToString();

        int r6 = UnityEngine.Random.Range(9, 10);
        c6.text = r6.ToString();
        sum = r1 + r2 + r3 + r4 + r5 + r6;
        wood.onClick.AddListener(woodSelected);
        fire.onClick.AddListener(fireSelected);
        water.onClick.AddListener(waterSelected);
        eagle.onClick.AddListener(eagleSelected);
        mountain.onClick.AddListener(mountainSelected);
        snake.onClick.AddListener(snakeSelected);
        brahmastra.onClick.AddListener(brahmastraSelected);
        brahmastra.GetComponent<Button>().interactable = false;
        villanblood = GameObject.Find("villanblood").GetComponent<Image>();
        playerblood = GameObject.Find("playerblood").GetComponent<Image>();


    }

    // Update is called once per frame

    IEnumerator waiter()
    {
        yield return new WaitForSeconds(0.5f);
        Throw();
       
    }
    IEnumerator waiteForSceneSwitchWithZero()
    {
        yield return new WaitForSeconds(1);
        Time.timeScale = 0f;
        lossScene.SetActive(true);
    }

    IEnumerator waitBackgrndRain()
    {
        yield return new WaitForSeconds(6f);
        //hangedBackground.SetBool("backgroundrain", false);
        backGround.SetBool("panelbackrain", false);
    }
    IEnumerator waiteForSceneSwitch()
    {
        yield return new WaitForSeconds(5);
        FindObjectOfType<coordination>().Onsingleplayerwin(3);
        Time.timeScale = 0f;
        winScene.SetActive(true);
    }
    public void Swipe()
    {
        if (Input.touches.Length > 0)
        {
            Touch t = Input.GetTouch(0);
            if (t.phase == TouchPhase.Began)
            {
                //save began touch 2d point
                firstPressPos = new Vector2(t.position.x, t.position.y);
            }
            if (t.phase == TouchPhase.Ended)
            {
                //save ended touch 2d point
                secondPressPos = new Vector2(t.position.x, t.position.y);

                //create vector from the two points
                currentSwipe = new Vector2(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);

                //normalize the 2d vector
                currentSwipe.Normalize();

                //swipe right
                if (currentSwipe.x > 0 && currentSwipe.y > 0 && arrowLoad.fillAmount==1.0f )//&& singleSwipeCheck)
                {
                    Debug.Log("right swipe");
                    //singleSwipeCheck = false;
                    //aklav.SetFloat("attack", 1.0f);
                    arrowLoad.fillAmount = 0.0f;
                    switch (arrowSelected)
                    {
                        case 1:
                            buttonHandler(woodarrow, c1, wood); //wood
                            break;
                        case 2:
                            buttonHandler(eaglearrow, c2, eagle);
                            break;
                        case 3:
                            buttonHandler(firearrow, c3, fire);
                            break;
                        case 4:
                            buttonHandler(waterarrow, c4, water);
                            break;
                        case 5:
                            buttonHandler(mountainarrow, c5, mountain);
                            break;
                        case 6:
                            buttonHandler(snakearrow, c6, snake);
                            break;
                      
                    }
                }
            }
        }
    }

    void Update()
    {
       
        arrowLoad.fillAmount += 1.0f / 4.0f * Time.deltaTime;
       
        float horizontal = Input.GetAxis("Horizontal");
        playerstrength = Convert.ToInt32((playerblood.fillAmount) * 100.0f);
        villanstrength = Convert.ToInt32((villanblood.fillAmount) * 100.0f);
        playerbloodper.text = playerstrength.ToString();
        villanbloodper.text = villanstrength.ToString();
        //test=GetComponent<Image>();
        Swipe();
        if(playerstrength==0)
        {
            StartCoroutine(waiteForSceneSwitchWithZero());
            
        }
        if (horizontal > 0)
        {
            Flip(horizontal);
            aklav.SetFloat("speed", 1.0f);

            Vector3 position = this.transform.position;
            position.x += 0.05f;
            this.transform.position = position;

        }
        else if (horizontal < 0)
        {
            aklav.SetFloat("speed", 1.0f);

            Flip(horizontal);
            Vector3 position = this.transform.position;
            position.x -= 0.05f;
            this.transform.position = position;

        }
        else
            aklav.SetFloat("speed", 0.0f);

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            Vector3 position = this.transform.position;
            position.y += 2;
            this.transform.position = position;

        }
        if (villanstrength < 10 && playerstrength >40 )
        {
            brahmastra.GetComponent<Button>().interactable = true;
            //changedBackground.SetBool("backgroundrain", true);
            backGround.SetBool("panelbackrain", true);
            StartCoroutine(waitBackgrndRain());
            
        }
        if((villanstrength<50&&villanstrength>45)||(playerstrength==villanstrength))
        {
            // changedBackground.SetBool("backgroundrain", true);
            backGround.SetBool("panelbackrain", true);
            StartCoroutine(waitBackgrndRain());
        }

    }
    private void OnCollisionEnter2D(Collision2D other)
    {
        print(other.gameObject.name);
        if (other.gameObject.name == "brahmastraarrow1(Clone)")
            Destroy(this.gameObject);
    }
    void woodSelected()
    {
        arrow = woodarrow;
        Debug.Log("wood selected");
        arrowSelected = 1;

    }
    void brahmastraSelected()
    {
        backGround.SetBool("panelbrahmastra", true);
        FindObjectOfType<audiomanager>().Play("brahmastra");
        arrowmm = brahmastraarrow; 
        aklav.SetTrigger("attack");
        StartCoroutine(waiter());
        //Throw();
        StartCoroutine(waiteForSceneSwitch());
        
    }
    void eagleSelected()
    {
        arrow = eaglearrow;
        Debug.Log("eagle selected");
        arrowSelected = 2;
    }
    void fireSelected()
    {
        arrow = firearrow;
        Debug.Log("fire selected");
        arrowSelected = 3;
    }
    void waterSelected()
    {
        arrow = waterarrow;
        Debug.Log("water selected");
        arrowSelected = 4;
    }
    void mountainSelected()
    {
        arrow = mountainarrow;
        Debug.Log("mountain selected");
        arrowSelected = 5;
    }
    void snakeSelected()
    {
        arrow = snakearrow;
        Debug.Log("snake selected");
        arrowSelected = 6;
    }
    void Throw()
    {
        sum--;
       
        //status.text = arrowmm.name;
        float xdistance;
        xdistance = target.position.x - throwPoint.position.x;

        float ydistance;
        ydistance = target.position.y - throwPoint.position.y;

        float throwAngle; // in radian
                          //OLD
                          //throwAngle = Mathf.Atan ((ydistance + 4.905f) / xdistance);
                          //UPDATED
        throwAngle = Mathf.Atan((ydistance + 4.905f * (timeTillHit * timeTillHit)) / xdistance);
        //OLD
        //float totalVelo = xdistance / Mathf.Cos(throwAngle) ;
        //UPDATED
        float totalVelo = xdistance / (Mathf.Cos(throwAngle) * timeTillHit);

        float xVelo, yVelo;
        xVelo = totalVelo * Mathf.Cos(throwAngle);
        yVelo = totalVelo * Mathf.Sin(throwAngle);

        GameObject bulletInstance = Instantiate(arrowmm, throwPoint.position, Quaternion.Euler(new Vector3(0, 0, 0))) as GameObject;
        Rigidbody2D rigid;
        rigid = bulletInstance.GetComponent<Rigidbody2D>();

        rigid.velocity = new Vector2(xVelo, yVelo);
        
        //singleSwipeCheck = true;

    }
    private void Flip(float horizontal)
    {
        if (horizontal > 0 && !facingRight || horizontal < 0 && facingRight)
        {
            facingRight = !facingRight;
            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
        }

    }

    
    void buttonHandler(GameObject arrowType, Text c, Button b)
    {
        // countofCurrentArrow = System.Convert.ToInt32(c.text);
        if (System.Convert.ToInt32(c.text) < 1)
        {
            b.GetComponent<Button>().interactable = false;
            b.spriteState = nullSprite;
            //singleSwipeCheck = true;            
        }
        else
        {


            c.text = (System.Convert.ToInt32(c.text) - 1).ToString();
            aklav.SetTrigger("attack");
            arrowmm = arrowType;
            StartCoroutine(waiter());
                       
            //status.text = "throw called";
        }

    }
}





