﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class brahmastradestroy : MonoBehaviour
{
    public Image villanblood, playerblood;
    public Animator animatonObject;
    public Transform positionOfAnim;
    void Start()
    {
        positionOfAnim = GameObject.Find("animationObject").GetComponent<Transform>();
        villanblood = GameObject.Find("villanblood").GetComponent<Image>();
        playerblood = GameObject.Find("playerblood").GetComponent<Image>();
        animatonObject = GameObject.Find("animationObject").GetComponent<Animator>();
        Destroy(gameObject, 3f);
    }
    IEnumerator waiter()
    {
        yield return new WaitForSeconds(3);
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        Debug.Log("i got collided with");

        Debug.Log(other.gameObject.name);
        if (other.gameObject.name != "brahmastraarrow(Clone)" && other.gameObject.name != "rightsurface" && other.gameObject.name != "EVIL")
        {
          

            Destroy(other.gameObject);
            playerblood.fillAmount = 0.0f;
           // Destroy(this.gameObject);
            StartCoroutine(waiter());

        }
        else if (other.gameObject.name == "brahmastraarrow(Clone)")
        {
            Destroy(this.gameObject);

        }
      
    }
    void animationOnCOlpos(float x, float y)
    {
        Vector3 a = positionOfAnim.transform.position;
        a.x = x;
        a.y = y;
        positionOfAnim.transform.position = a;

    }
}
