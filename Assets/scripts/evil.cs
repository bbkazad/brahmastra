﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class evil : MonoBehaviour {
    private Animator evilk, backGround;
    public Transform target;
    public Transform throwPoint;

    public GameObject ball,lossScene;

    public float timeTillHit = 1f;

    public static int woods;
    public static int snakes;
    public static int fires;
    public static int waters;
    public static int mountains;
    public static int eagles;
    public static int sum;

    public Sprite woodssprite;
    public Sprite snakessprite;
    public Sprite firessprite;
    public Sprite waterssprite;
    public Sprite mountainssprite;
    public Sprite eaglessprite,brahmastrasprite;

    public IList<int> listPicker;
    public static Image evil_asset,permanentsword, permanentswordblood;
    public static System.Random rnd;
    public bool waiting,throwCheck,middletimechecker;
    

    public GameObject woodarrow, firearrow, eaglearrow, waterarrow, mountainarrow, snakearrow,brahmastraarrow;


    // Use this for initialization
    void Start () {
        throwCheck = true;
        middletimechecker = false;
        print("is it going again");
        rnd = new System.Random();
        waiting = false;
        listPicker= new List<int>();
        evil_asset = GameObject.Find("evil_asset").GetComponent<Image>();
        permanentsword = GameObject.Find("permanentsword").GetComponent<Image>();
        permanentswordblood = GameObject.Find("permanentswordblood").GetComponent<Image>();
        evilk = GetComponent<Animator>();
        backGround = GameObject.Find("backGround").GetComponent<Animator>();
        woods = UnityEngine.Random.Range(5, 10);
        snakes = UnityEngine.Random.Range(10,12);
        fires = UnityEngine.Random.Range(12,15);
        waters = UnityEngine.Random.Range(12,15);
        mountains = UnityEngine.Random.Range(13, 17);
        eagles = UnityEngine.Random.Range(12,15);

        sum = woods + snakes + fires + waters + mountains + eagles;
        print("The sum selected is "+sum);
        print("wood-{0} snake-{1} fire-{2} water-{3} mountain-{4} eagle-{5}"+ woods+ snakes+ fires+ waters+ mountains+ eagles);
        if (woods > 0) listPicker.Add(1);
        if (snakes > 0) listPicker.Add(2);
        if (fires > 0) listPicker.Add(3);
        if (waters > 0) listPicker.Add(4);
        if (mountains > 0) listPicker.Add(5);
        if (eagles > 0) listPicker.Add(6);
        permanentsword.enabled = false;
        permanentswordblood.enabled = false;
    }

    IEnumerator waiter()
    {
        yield return new WaitForSeconds(5);
        waiting = false;
    }
    IEnumerator waitmiddletime()//method to wait some seconds to shoot
    {
        yield return new WaitForSeconds(3);
        evilk.SetTrigger("attack");
        StartCoroutine(waitForAnim());
        //middletimechecker = true;
    }
    IEnumerator waitForAnim()
    {
        yield return new WaitForSeconds(0.5f);
        Throw();
    }
    IEnumerator waitForSceneSwitch()
    {
        yield return new WaitForSeconds(7);
        Time.timeScale = 0f;
        lossScene.SetActive(true);
        
    }

    // Update is called once per frame
    void Update () {
        if (waiting == false)
        {
            waiting = true;
            if (sum > 0)
            {
                print("waiting for 5 secs?");
                StartCoroutine(waiter());
                print("Now sum is  " + sum);
                int arrow_select = listPicker[rnd.Next(listPicker.Count)];
                print("arrow selected is " + arrow_select);
                print("previous wood-{0} snake-{1} fire-{2} water-{3} mountain-{4} eagle-{5}" + woods + " " + snakes + " " + fires + " " + waters + " " + mountains + " " + eagles);
                switch (arrow_select)
                {
                    case 1:
                        spriteSelecter(ref woods, ref woodssprite, 1, woodarrow);
                        break;
                    case 2:
                        spriteSelecter(ref snakes, ref snakessprite, 2, snakearrow);
                        break;
                    case 3:
                        spriteSelecter(ref fires, ref firessprite, 3, firearrow);
                        break;
                    case 4:
                        spriteSelecter(ref waters, ref waterssprite, 4, waterarrow);
                        break;
                    case 5:
                        spriteSelecter(ref mountains, ref mountainssprite, 5, mountainarrow);
                        break;
                    case 6:
                        spriteSelecter(ref eagles, ref eaglessprite, 6, eaglearrow);
                        break;
                }
                print("after operation wood-{0} snake-{1} fire-{2} water-{3} mountain-{4} eagle-{5}" + woods + " " + snakes + " " + fires + " " + waters + " " + mountains + " " + eagles);
            }
            //else print("SUM IS 0");
           

        }
        if (throwCheck && css.playerstrength < 5 && css.playerstrength!=0)
        {
            backGround.SetBool("panelbrahmastra", true);
            FindObjectOfType<audiomanager>().Play("brahmastra");
            throwCheck = false;
            sum = 0;
            ball = brahmastraarrow;
            evil_asset.sprite = brahmastrasprite;
            //Throw();
            StartCoroutine(waitForSceneSwitch());
        }
        if((css.villanstrength<10 && css.playerstrength>40)||css.villanstrength<10)
        {
            permanentsword.enabled = true;
            permanentswordblood.enabled = true;

        }

    }
    void spriteSelecter(ref int arrowCount, ref Sprite arrow, int itemNumber, GameObject arrowgameobject)
{
        evil_asset.sprite = arrow;
        arrowCount -= 1;
        sum -= 1;
        if (arrowCount == 0)
            listPicker.Remove(itemNumber);
        ball = arrowgameobject;
        StartCoroutine(waitmiddletime());
} 
    void Throw()
    {
        float xdistance;
        xdistance = target.position.x - throwPoint.position.x;

        float ydistance;
        ydistance = target.position.y - throwPoint.position.y;

        float throwAngle; // in radian
                          //OLD
                          //throwAngle = Mathf.Atan ((ydistance + 4.905f) / xdistance);
                          //UPDATED
        throwAngle = Mathf.Atan((ydistance + 4.905f * (timeTillHit * timeTillHit)) / xdistance);
        //OLD
        //float totalVelo = xdistance / Mathf.Cos(throwAngle) ;
        //UPDATED
        float totalVelo = xdistance / (Mathf.Cos(throwAngle) * timeTillHit);

        float xVelo, yVelo;
        xVelo = totalVelo * Mathf.Cos(throwAngle);
        yVelo = totalVelo * Mathf.Sin(throwAngle);

        GameObject bulletInstance = Instantiate(ball, throwPoint.position, Quaternion.Euler(new Vector3(0, 0, 0))) as GameObject;
        Rigidbody2D rigid;
        rigid = bulletInstance.GetComponent<Rigidbody2D>();

        rigid.velocity = new Vector2(xVelo, yVelo);
    }
}
