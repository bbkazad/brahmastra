﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class wood_destroy1 : MonoBehaviour
{
    public Image villanblood, playerblood;
    public Animator animatonObject;
    public Transform positionOfAnim;
    void Start()
    {
        positionOfAnim = GameObject.Find("animationObject").GetComponent<Transform>();
        animatonObject = GameObject.Find("animationObject").GetComponent<Animator>();
        villanblood = GameObject.Find("villanblood").GetComponent<Image>();
        playerblood = GameObject.Find("playerblood").GetComponent<Image>();
        Destroy(gameObject, 3f);
    }
    IEnumerator waiter()
    {
        yield return new WaitForSeconds(3);
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        Debug.Log("i got collided with");

        Debug.Log(other.gameObject.name);
        if (other.gameObject.name != "Aklav" && other.gameObject.name != "leftsurface" && other.gameObject.name != "EVIL" && other.gameObject.name != "brahmastraarrow(Clone)")
        {
            if (other.gameObject.name == "woods(Clone)")
            {
                animationOnCOlpos(this.gameObject.transform.position.x, this.gameObject.transform.position.y);
                animatonObject.SetTrigger("woodwood");
                // FindObjectOfType<audiomanager>().Play("water");
            }
            else if (other.gameObject.name == "snakes(Clone)")
            {
                villanblood.fillAmount -= 0.08f;
                animationOnCOlpos(this.gameObject.transform.position.x, this.gameObject.transform.position.y);
                animatonObject.SetTrigger("snakewood");
                FindObjectOfType<audiomanager>().Play("snake");
            }
            else if (other.gameObject.name == "fires(Clone)")
            {
                villanblood.fillAmount -= 0.08f;
                animationOnCOlpos(this.gameObject.transform.position.x, this.gameObject.transform.position.y);
                animatonObject.SetTrigger("firewood");
                FindObjectOfType<audiomanager>().Play("fire");
            }
            else if (other.gameObject.name == "waters(Clone)")
            {
                villanblood.fillAmount -= 0.08f;
                animationOnCOlpos(this.gameObject.transform.position.x, this.gameObject.transform.position.y);
                animatonObject.SetTrigger("waterwood");
                FindObjectOfType<audiomanager>().Play("water");
            }
            else if (other.gameObject.name == "mountains(Clone)")
            {
                villanblood.fillAmount -= 0.08f;
                animationOnCOlpos(this.gameObject.transform.position.x, this.gameObject.transform.position.y);
                animatonObject.SetTrigger("mountainwood");
                FindObjectOfType<audiomanager>().Play("mountain");
            }
            else if (other.gameObject.name == "eagles(Clone)")
            {
                villanblood.fillAmount -= 0.08f;
                animationOnCOlpos(this.gameObject.transform.position.x, this.gameObject.transform.position.y);
                animatonObject.SetTrigger("eaglewood");
                FindObjectOfType<audiomanager>().Play("eagle");
            }
            else
            {  }
            
            Destroy(other.gameObject);
            Destroy(this.gameObject);
            StartCoroutine(waiter());
            
        }
        else if(other.gameObject.name == "Aklav" || other.gameObject.name == "leftsurface")
        {
            Destroy(this.gameObject);
            playerblood.fillAmount -= 0.1f;

        }
        
    }
    void animationOnCOlpos(float x, float y)
    {
        Vector3 a = positionOfAnim.transform.position;
        a.x = x;
        a.y = y;
        positionOfAnim.transform.position = a;

    }

}
