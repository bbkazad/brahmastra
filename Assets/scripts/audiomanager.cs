﻿using UnityEngine.Audio;
using System;
using UnityEngine;

public class audiomanager : MonoBehaviour {

    public sound[] sounds;
    public AudioMixer main;
	// Use this for initialization
	void Awake () {
		foreach(sound s in sounds)
        {
          s.source=  gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
        }

	}
    public void Volumeon()
    {
       foreach(sound s in sounds)
        {
            //s.volume = 1.0f;
            s.source.volume = 1.0f;
        }
    }
    public void Volumeoff()
    {
        foreach (sound s in sounds)
        {
            //s.volume = 0.0f;
            s.source.volume = 0.0f;
        }
    }
    public void Play(string name)
    {
        sound s = Array.Find(sounds, sound => sound.name == name);
        s.source.Play();
    }
}
