﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class arrow_destroy : MonoBehaviour {
    public Image villanblood;
   
    void Start()
    {
        villanblood = GameObject.Find("villanblood").GetComponent<Image>();
        Destroy(gameObject, 3f);
    }
    void OnCollisionEnter2D(Collision2D bbk)
    {   
        if (bbk.gameObject.name == "EVIL" || bbk.gameObject.name == "rightsurface")
        {
            villanblood.fillAmount -= 0.1f;
            Destroy(this.gameObject);
        }          
    }
}
