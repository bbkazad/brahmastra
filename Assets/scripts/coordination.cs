﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class coordination : MonoBehaviour {
    public GameObject pauseMenuUI;
    public int coins;
    public Text coinscount;
    public GameObject buyblood;
    void Start()
    {
        coins = PlayerPrefs.GetInt("Money");
        print(coins);
    }
    // Use this for initialization
    public void Paused()
    {
        Time.timeScale = 0f;
        pauseMenuUI.SetActive(true);
    }
    public void Resume()
    {
        Time.timeScale = 1f;
        pauseMenuUI.SetActive(false);
    }
    public void Mainmenu()
    {
        SceneManager.LoadScene("menubar");
    }
    public void Retry()
    {
        SceneManager.LoadScene("game");
        Time.timeScale = 1f;
        //css.singleSwipeCheck = true;
    }
    public void RetryMultiPlayer()
    {
        SceneManager.LoadScene("multiplayer");
        Time.timeScale = 1f;
        //multiplayer1.mulsingleSwipeCheck = true;
        //multiplayer2.mulsingleSwipeCheck1 = true;
    }
    public void Volumeon()
    {
        FindObjectOfType<audiomanager>().Volumeon();
    }
    public void Volumeoff()
    {
        FindObjectOfType<audiomanager>().Volumeoff();
    }
    public void Onsingleplayerwin(int num)
    {
        coins += num;
        coinscount.text = coins.ToString();
        PlayerPrefs.SetInt("Money", coins);
        PlayerPrefs.Save();
    }
    public void Buyblood()
    {
        Time.timeScale = 0f;
        buyblood.SetActive(true);
    }
    public void BuybloodExit()
    {
        buyblood.SetActive(false);
        Time.timeScale = 1f;
    }
    public void buyBloodWithCoins()
    {
        coins = PlayerPrefs.GetInt("Money");
        if(coins>=5 && css.playerstrength<100)
        {
            css.playerstrength += 10;
            css.playerblood.fillAmount += 0.1f;
            coins -= 5;
            PlayerPrefs.SetInt("Money", coins);
            PlayerPrefs.Save();
        }
    }
    void Update()
    {
        coinscount.text = coins.ToString();
    }


}
