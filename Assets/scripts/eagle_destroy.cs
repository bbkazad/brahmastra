﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class eagle_destroy : MonoBehaviour
{
    public Image villanblood, playerblood;
    public Animator animatonObject;
    public Transform positionOfAnim;
    void Start()
    {
        positionOfAnim = GameObject.Find("animationObject").GetComponent<Transform>();
        villanblood = GameObject.Find("villanblood").GetComponent<Image>();
        playerblood = GameObject.Find("playerblood").GetComponent<Image>();
        Destroy(gameObject, 3f);
        animatonObject = GameObject.Find("animationObject").GetComponent<Animator>();

    }
    IEnumerator waiter()
    {
        yield return new WaitForSeconds(3);
    }

    void OnCollisionEnter2D(Collision2D other) //wood-{1} snake-{2} fire-{3} water-{4} mountain-{5} eagle-{6}
    {
        Debug.Log("i got collided with");

        Debug.Log(other.gameObject.name);
        if (other.gameObject.name != "Aklav" && other.gameObject.name != "leftsurface" && other.gameObject.name != "EVIL" && other.gameObject.name != "brahmastraarrow(Clone)")
        {
            if(other.gameObject.name=="woods(Clone)")
            {
                animationOnCOlpos(this.gameObject.transform.position.x, this.gameObject.transform.position.y);
                playerblood.fillAmount -= 0.08f;
                animatonObject.SetTrigger("woodeagle");
                FindObjectOfType<audiomanager>().Play("eagle");

            }
            else if (other.gameObject.name == "snakes(Clone)")
            {
                playerblood.fillAmount -= 0.04f;
                animationOnCOlpos(this.gameObject.transform.position.x, this.gameObject.transform.position.y);
                animatonObject.SetTrigger("snakeeagle");
                FindObjectOfType<audiomanager>().Play("eagle");
            }
            else if (other.gameObject.name == "fires(Clone)")
            {
                villanblood.fillAmount -= 0.02f;
                animationOnCOlpos(this.gameObject.transform.position.x, this.gameObject.transform.position.y);
                animatonObject.SetTrigger("fireeagle");
                FindObjectOfType<audiomanager>().Play("fire");
            }
            else if (other.gameObject.name == "waters(Clone)")
            {
                villanblood.fillAmount -= 0.02f;
                animationOnCOlpos(this.gameObject.transform.position.x, this.gameObject.transform.position.y);
                animatonObject.SetTrigger("watereagle");
                FindObjectOfType<audiomanager>().Play("water");
            }
            else if (other.gameObject.name == "mountains(Clone)")
            {
                villanblood.fillAmount -= 0.02f;
                animationOnCOlpos(this.gameObject.transform.position.x, this.gameObject.transform.position.y);
                animatonObject.SetTrigger("mountaineagle");
                FindObjectOfType<audiomanager>().Play("mountain");
            }
            else if (other.gameObject.name == "eagles(Clone)")
            {
                //do nothing
                animationOnCOlpos(this.gameObject.transform.position.x, this.gameObject.transform.position.y);
                animatonObject.SetTrigger("eagleeagle");
                FindObjectOfType<audiomanager>().Play("eagle");
            }
            else
            { }

            Destroy(other.gameObject);
            Destroy(this.gameObject);
            StartCoroutine(waiter());
        }
        else if (other.gameObject.name == "Aklav" || other.gameObject.name == "leftsurface")
        {
            Destroy(this.gameObject);
            playerblood.fillAmount -= 0.1f;


        }
       
        
    }
    void animationOnCOlpos(float x, float y)
    {
        Vector3 a = positionOfAnim.transform.position;
        a.x = x;
        a.y = y;
        positionOfAnim.transform.position = a;

    }
}
