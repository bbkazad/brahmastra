﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class mainbarscript : MonoBehaviour {
    public AudioSource sound;
	public void Playgame()
    {
        
    }
    public void Quit()
    {
        Application.Quit();
    }
    public void Singleplayer()
    {
        SceneManager.LoadScene("game");
        Time.timeScale = 1f;
    }
    public void Multiplayer()
    {
        SceneManager.LoadScene("multiplayer");
        Time.timeScale = 1f;
    }
    
    public void volumeOn()
    {
        sound.volume = 1.0f;
    }
    public void volumeOff()
    {
        sound.volume = 0.0f;
    }
}
