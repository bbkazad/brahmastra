﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
public class mulmountain_destroy1 : MonoBehaviour
{
    public Image player2blood, player1blood;
    public Animator mulAnimationObject;
    public Transform positionOfAnim;
    void Start()
    {
        positionOfAnim = GameObject.Find("mulAnimationObject").GetComponent<Transform>();
        player2blood = GameObject.Find("player2blood").GetComponent<Image>();
        player1blood = GameObject.Find("player1blood").GetComponent<Image>();
        mulAnimationObject = GameObject.Find("mulAnimationObject").GetComponent<Animator>();
        Destroy(gameObject, 3f);
    }
    IEnumerator waiter()
    {
        yield return new WaitForSeconds(3);
    }
   
    void OnCollisionEnter2D(Collision2D other)
    {
        #region "collisionprogram"
        Debug.Log("i got collided withhh");
               
        Debug.Log(other.gameObject.name);
        if (other.gameObject.name != "player1" && other.gameObject.name != "leftsurface" && other.gameObject.name != "player2" && other.gameObject.name != "mulbrahmastraarrow(Clone)")
        {
            if (other.gameObject.name == "mulwoods(Clone)")
            {
                animationOnCOlpos(this.gameObject.transform.position.x, this.gameObject.transform.position.y);
                player1blood.fillAmount -= 0.08f;
                mulAnimationObject.SetTrigger("mulwoodmountain");
                FindObjectOfType<audiomanager>().Play("mountain");

            }
            else if (other.gameObject.name == "mulsnakes(Clone)")
            {
                player1blood.fillAmount -= 0.02f;
                animationOnCOlpos(this.gameObject.transform.position.x, this.gameObject.transform.position.y);
                mulAnimationObject.SetTrigger("mulsnakemountain");
                FindObjectOfType<audiomanager>().Play("mountain");
            }
            else if (other.gameObject.name == "mulfires(Clone)")
            {
                player1blood.fillAmount -= 0.02f;
                animationOnCOlpos(this.gameObject.transform.position.x, this.gameObject.transform.position.y);
                mulAnimationObject.SetTrigger("mulfiremountain");
                FindObjectOfType<audiomanager>().Play("mountain");
            }
            else if (other.gameObject.name == "mulwaters(Clone)")
            {
                player2blood.fillAmount -= 0.02f;
                animationOnCOlpos(this.gameObject.transform.position.x, this.gameObject.transform.position.y);
                mulAnimationObject.SetTrigger("mulwatermountain");
                FindObjectOfType<audiomanager>().Play("water");
            }
            else if (other.gameObject.name == "mulmountains(Clone)")
            {
                //do  nothing
                animationOnCOlpos(this.gameObject.transform.position.x, this.gameObject.transform.position.y);
                mulAnimationObject.SetTrigger("mulmountainmountain");
                FindObjectOfType<audiomanager>().Play("mountain");
            }
            else if(other.gameObject.name ==  "muleagles(Clone)")
            {
                player1blood.fillAmount -= 0.02f;
                animationOnCOlpos(this.gameObject.transform.position.x, this.gameObject.transform.position.y);
                mulAnimationObject.SetTrigger("muleaglemountain");
                FindObjectOfType<audiomanager>().Play("mountain");
            }
            

            Destroy(other.gameObject);
            Destroy(this.gameObject);
            StartCoroutine(waiter());
        }
        else if (other.gameObject.name == "player1" || other.gameObject.name == "leftsurface")
        {
            Destroy(this.gameObject);
            player1blood.fillAmount -= 0.1f;

        }

        #endregion
    }

    void animationOnCOlpos(float x, float y )
    {
        Vector3 a =positionOfAnim.transform.position;
        a.x = x;
        a.y = y;
        positionOfAnim.transform.position = a;

    }


}