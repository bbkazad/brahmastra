﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class mulsnake_destroy1 : MonoBehaviour
{
    public Image player2blood, player1blood;
    public Animator mulAnimationObject;
    public Transform positionOfAnim;
    void Start()
    {
        positionOfAnim = GameObject.Find("mulAnimationObject").GetComponent<Transform>();
        player2blood = GameObject.Find("player2blood").GetComponent<Image>();
        player1blood = GameObject.Find("player1blood").GetComponent<Image>();
        mulAnimationObject = GameObject.Find("mulAnimationObject").GetComponent<Animator>();
        Destroy(gameObject, 3f);
    }
    IEnumerator waiter()
    {
        yield return new WaitForSeconds(3);
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        Debug.Log("i got collided with");

        Debug.Log(other.gameObject.name);
        if (other.gameObject.name != "player1" && other.gameObject.name != "leftsurface" && other.gameObject.name != "player2" && other.gameObject.name != "mulbrahmastraarrow(Clone)")
        {
            if (other.gameObject.name == "mulwoods(Clone)")
            {
                animationOnCOlpos(this.gameObject.transform.position.x, this.gameObject.transform.position.y);
                player1blood.fillAmount -= 0.08f;
                mulAnimationObject.SetTrigger("mulwoodsnake");
                FindObjectOfType<audiomanager>().Play("snake");

            }
            else if (other.gameObject.name == "mulsnakes(Clone)")
            {
                //do nothing
                animationOnCOlpos(this.gameObject.transform.position.x, this.gameObject.transform.position.y);
                mulAnimationObject.SetTrigger("mulsnakesnake");
                FindObjectOfType<audiomanager>().Play("snake");
            }
            else if (other.gameObject.name == "mulfires(Clone)")
            {
                player2blood.fillAmount -= 0.02f;
                animationOnCOlpos(this.gameObject.transform.position.x, this.gameObject.transform.position.y);
                mulAnimationObject.SetTrigger("mulfiresnake");
                FindObjectOfType<audiomanager>().Play("fire");
            }
            else if (other.gameObject.name == "mulwaters(Clone)")
            {
                player1blood.fillAmount -= 0.02f;
                animationOnCOlpos(this.gameObject.transform.position.x, this.gameObject.transform.position.y);
                mulAnimationObject.SetTrigger("mulwatersnake");
                FindObjectOfType<audiomanager>().Play("water");
            }
            else if (other.gameObject.name == "mulmountains(Clone)")
            {
                player2blood.fillAmount -= 0.02f;
                animationOnCOlpos(this.gameObject.transform.position.x, this.gameObject.transform.position.y);
                mulAnimationObject.SetTrigger("mulmountainsnake");
                FindObjectOfType<audiomanager>().Play("mountain");
            }
            else if (other.gameObject.name == "muleagles(Clone)")
            {
                player2blood.fillAmount -= 0.04f;
                animationOnCOlpos(this.gameObject.transform.position.x, this.gameObject.transform.position.y);
                mulAnimationObject.SetTrigger("muleaglesnake");
                FindObjectOfType<audiomanager>().Play("eagle");
            }

            Destroy(other.gameObject);
            Destroy(this.gameObject);
            StartCoroutine(waiter());
        }
        else if (other.gameObject.name == "player1" || other.gameObject.name == "leftsurface")
        {
            Destroy(this.gameObject);
            player1blood.fillAmount -= 0.1f;

        }

    }
    void animationOnCOlpos(float x, float y)
    {
        Vector3 a = positionOfAnim.transform.position;
        a.x = x;
        a.y = y;
        positionOfAnim.transform.position = a;

    }
}
