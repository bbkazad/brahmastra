﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;
using UnityEngine.SceneManagement;


public class multiplayer2 : MonoBehaviour
{
    
    private Animator player2, mulBackGround;
    public static bool facingRight;// mulsingleSwipeCheck1;
    public Transform target;
    public Transform throwPoint;
    public GameObject arrow,player2WinScene;
    public Image mularrowLoad1;
    //public Image test;
    public float timeTillHit = 1f;
    public Button wood1, fire1, eagle1, water1, mountain1, snake1,brahmastra1;
    public GameObject woodarrow1, firearrow1, eaglearrow1, waterarrow1, mountainarrow1, snakearrow1;
    public GameObject brahmastraarrow1,arrowmm;
    public Text c11, c21, c31, c41, c51, c61, wontext;
    public int arrowSelected;
    public SpriteState nullSprite;
    
    
    public static int sum;

    public Vector2 firstPressPos;
    public Vector2 secondPressPos;
    public Vector2 currentSwipe;

    
    //public evil evilobject;

    void Start()
    {
       
        player2 = GetComponent<Animator>();
        mulBackGround = GameObject.Find("mulBackGround").GetComponent<Animator>();
        //test= GameObject.Find("test").GetComponent<Image>();
        wood1 = wood1.GetComponent<Button>();
        fire1 = fire1.GetComponent<Button>();
        water1 = water1.GetComponent<Button>();
        mountain1 = mountain1.GetComponent<Button>();
        eagle1 = eagle1.GetComponent<Button>();
        snake1 = snake1.GetComponent<Button>();
        brahmastra1 = brahmastra1.GetComponent<Button>();
        //utton click = clickk.GetComponent<Button>();
        //utton click = clickk.GetComponent<Button>();
        //click.onClick.AddListener(attacking);

        facingRight = true;

        c11 = GameObject.Find("c11").GetComponent<Text>();
        c21= GameObject.Find("c21").GetComponent<Text>();
        c31= GameObject.Find("c31").GetComponent<Text>();
        c41= GameObject.Find("c41").GetComponent<Text>();
        c51= GameObject.Find("c51").GetComponent<Text>();
        c61= GameObject.Find("c61").GetComponent<Text>();
        //wontext = GameObject.Find("wontext").GetComponent<Text>();
        mularrowLoad1 = GameObject.Find("mularrowLoad1").GetComponent<Image>();



        int r1 = UnityEngine.Random.Range(9, 12);
        c11.text = r1.ToString();

        int r2 = UnityEngine.Random.Range(9, 12);
        c21.text = r2.ToString();

        int r3 = UnityEngine.Random.Range(9, 12);
        c31.text = r3.ToString();

        int r4 = UnityEngine.Random.Range(8, 11);
        c41.text = r4.ToString();

        int r5 = UnityEngine.Random.Range(8, 11);
        c51.text = r5.ToString();

        int r6 = UnityEngine.Random.Range(10, 13);
        c61.text = r6.ToString();
        sum = r1 + r2 + r3 + r4 + r5 + r6;
        wood1.onClick.AddListener(woodSelected);
        fire1.onClick.AddListener(fireSelected);
        water1.onClick.AddListener(waterSelected);
        eagle1.onClick.AddListener(eagleSelected);
        mountain1.onClick.AddListener(mountainSelected);
        snake1.onClick.AddListener(snakeSelected);
        brahmastra1.onClick.AddListener(brahmastraSelected);
        brahmastra1.GetComponent<Button>().interactable = false;
        //mulsingleSwipeCheck1 = true;
        }

    // Update is called once per frame

    IEnumerator waiter()
    {
        yield return new WaitForSeconds(0.5f);
        Throw();
    }
    IEnumerator waiteForRain()
    {
        yield return new WaitForSeconds(6f);
        mulBackGround.SetBool("mulpanelbackrain", false);
    }
    IEnumerator waiteForSceneSwitch()
    {
        yield return new WaitForSeconds(3);
        mulBackGround.SetBool("mulpanelbrahmastra", false);
        brahmastra1.GetComponent<Button>().interactable = false;
        Time.timeScale = 0f;
        player2WinScene.SetActive(true);
    }
    IEnumerator waiteForSceneSwitchWithZero()
    {
        yield return new WaitForSeconds(1);
        Time.timeScale = 0f;
        player2WinScene.SetActive(true);
    }
    public void Swipe()
    {
        if (Input.touches.Length > 0)
        {
            Touch t = Input.GetTouch(0);
            if (t.phase == TouchPhase.Began)
            {
                //save began touch 2d point
                firstPressPos = new Vector2(t.position.x, t.position.y);
            }
            if (t.phase == TouchPhase.Ended)
            {
                //save ended touch 2d point
                secondPressPos = new Vector2(t.position.x, t.position.y);

                //create vector from the two points
                currentSwipe = new Vector2(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);

                //normalize the 2d vector
                currentSwipe.Normalize();

                //swipe right
                if (currentSwipe.x < 0 && currentSwipe.y > 0 && mularrowLoad1.fillAmount == 1.0f) //&& mulsingleSwipeCheck1)
                {
                    mularrowLoad1.fillAmount = 0.0f;
                    //mulsingleSwipeCheck1 = false;
                    Debug.Log("right swipe");
                    //player2.SetFloat("attack", 1.0f);
                    switch (arrowSelected)
                    {
                        case 1:
                            buttonHandler(woodarrow1, c11, wood1); //wood
                            break;
                        case 2:
                            buttonHandler(eaglearrow1, c21, eagle1);
                            break;
                        case 3:
                            buttonHandler(firearrow1, c31, fire1);
                            break;
                        case 4:
                            buttonHandler(waterarrow1, c41, water1);
                            break;
                        case 5:
                            buttonHandler(mountainarrow1, c51, mountain1);
                            break;
                        case 6:
                            buttonHandler(snakearrow1, c61, snake1);
                            break;
                      
                    }
                }
            }
        }
    }

    void Update()
    {
        mularrowLoad1.fillAmount += 1.0f / 3.0f * Time.deltaTime;
        float horizontal = Input.GetAxis("Horizontal");
      
        //test=GetComponent<Image>();
        Swipe();
       
        if (multiplayer1.player2strength-multiplayer1.player1strength > 40 )
        {
            brahmastra1.GetComponent<Button>().interactable = true;
            mulBackGround.SetBool("mulpanelbackrain", true);
            StartCoroutine(waiteForRain());

        }
        if(multiplayer1.player1blood.fillAmount<0.01)
        {
            // Time.timeScale = 0f;
            // player2WinScene.SetActive(true);
            StartCoroutine(waiteForSceneSwitchWithZero());
        }

    }
    private void OnCollisionEnter2D(Collision2D other)
    {
        print(other.gameObject.name);
        if (other.gameObject.name == "mulbrahmastraarrow(Clone)")
            Destroy(this.gameObject);
    }
    void woodSelected()
    {
        arrow = woodarrow1;
        Debug.Log("wood selected");
        arrowSelected = 1;

    }
    void brahmastraSelected()
    {
        mulBackGround.SetBool("mulpanelbrahmastra", true);
        FindObjectOfType<audiomanager>().Play("brahmastra");
        arrowmm = brahmastraarrow1;
        player2.SetTrigger("attack");
        //mulsingleSwipeCheck1 = false;
        StartCoroutine(waiter());
        StartCoroutine(waiteForSceneSwitch());
        
    }
    void eagleSelected()
    {
        arrow = eaglearrow1;
        Debug.Log("eagle selected");
        arrowSelected = 2;
    }
    void fireSelected()
    {
        arrow = firearrow1;
        Debug.Log("fire selected");
        arrowSelected = 3;
    }
    void waterSelected()
    {
        arrow = waterarrow1;
        Debug.Log("water selected");
        arrowSelected = 4;
        ;
    }
    void mountainSelected()
    {
        arrow = mountainarrow1;
        Debug.Log("mountain selected");
        arrowSelected = 5;
        ;
    }
    void snakeSelected()
    {
        arrow = snakearrow1;
        Debug.Log("snake selected");
        arrowSelected = 6;
        ;
    }
    void Throw()
    {
        sum--;
       
        //status.text = arrowmm.name;
        float xdistance;
        xdistance = target.position.x - throwPoint.position.x;

        float ydistance;
        ydistance = target.position.y - throwPoint.position.y;

        float throwAngle; // in radian
                          //OLD
                          //throwAngle = Mathf.Atan ((ydistance + 4.905f) / xdistance);
                          //UPDATED
        throwAngle = Mathf.Atan((ydistance + 4.905f * (timeTillHit * timeTillHit)) / xdistance);
        //OLD
        //float totalVelo = xdistance / Mathf.Cos(throwAngle) ;
        //UPDATED
        float totalVelo = xdistance / (Mathf.Cos(throwAngle) * timeTillHit);

        float xVelo, yVelo;
        xVelo = totalVelo * Mathf.Cos(throwAngle);
        yVelo = totalVelo * Mathf.Sin(throwAngle);

        GameObject bulletInstance = Instantiate(arrowmm, throwPoint.position, Quaternion.Euler(new Vector3(0, 0, 0))) as GameObject;
        Rigidbody2D rigid;
        rigid = bulletInstance.GetComponent<Rigidbody2D>();

        rigid.velocity = new Vector2(xVelo, yVelo);
        
        //mulsingleSwipeCheck1 = true;
    }
    private void Flip(float horizontal)
    {
        if (horizontal > 0 && !facingRight || horizontal < 0 && facingRight)
        {
            facingRight = !facingRight;
            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
        }

    }



    void buttonHandler(GameObject arrowType, Text c, Button b)
    {
        // countofCurrentArrow = System.Convert.ToInt32(c.text);
        if (System.Convert.ToInt32(c.text) < 1)
        {
            b.GetComponent<Button>().interactable = false;
            b.spriteState = nullSprite;
            //mulsingleSwipeCheck1 = true;
        }
        else
        {


            c.text = (System.Convert.ToInt32(c.text) - 1).ToString();
            player2.SetTrigger("attack");
            arrowmm = arrowType;
            StartCoroutine(waiter());
           
            
            //status.text = "throw called";
        }

    }
}





