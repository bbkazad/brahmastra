﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class mulplayerbrahmastra1 : MonoBehaviour
{
    public Image player1blood;
    // Use this for initialization
    void Start()
    {
        player1blood = GameObject.Find("player1blood").GetComponent<Image>();

    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.name != "player2" && other.gameObject.name != "rightsurface" && other.gameObject.name != "mulbrahmastraarrow1(Clone)")
        {
            Destroy(other.gameObject);
            player1blood.fillAmount = 0.0f;
          //  multiplayer1.player1strength = 0;

        }
        else if (other.gameObject.name == "mulbrahmastraarrow(Clone)")
        {
            Destroy(this.gameObject);

        }
    }
}
