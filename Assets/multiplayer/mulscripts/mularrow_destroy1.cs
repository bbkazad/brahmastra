﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class mularrow_destroy1 : MonoBehaviour {
    public Image player1blood;
    public GameObject player2blood;
    void Start()
    {
        player2blood = GameObject.Find("player2blood");
        player1blood = GameObject.Find("player1blood").GetComponent<Image>();
        Destroy(gameObject, 3f);
    }
    void OnCollisionEnter2D(Collision2D bbk)
    {   
        if (bbk.gameObject.name == "player2" || bbk.gameObject.name == "rightsurface")
        {
            player2blood.GetComponent<Image>().fillAmount -= 0.1f;
            print("..................."+this.gameObject.name);
            print("..................." + bbk.gameObject.name);

            Destroy(this.gameObject);
        }          
    }
}
