﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class coinsCollwction : MonoBehaviour {

    // Use this for initialization
    public int coins;
    public Text coinscount;
    //public int presentcoins=0;

	void Start () {
        coins = PlayerPrefs.GetInt("Money");
        coinscount.text = coins.ToString();
    }
    IEnumerator waiteForShare()
    {
        yield return new WaitForSeconds(7f);
        coins++;
        coinscount.text = coins.ToString();
        PlayerPrefs.SetInt("Money", coins);
    }

    // Update is called once per frame
    public void OnShare()
    {
        StartCoroutine(waiteForShare());
        PlayerPrefs.Save();
	}
}
